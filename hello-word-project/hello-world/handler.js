"use strict"

module.exports = (context, callback) => {
  callback(undefined, {
      token: process.env['TOKEN']
    , message: process.env['MESSAGE']
    , data: context.length>0 ? JSON.parse(context) : {}
    , path: process.env['Http_Path']
  })
}
